import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class DataSet implements Iterable{
    private List<Data> dataList;

    public DataSet(){
        dataList = new ArrayList<>();
    }

    public void addData(Data data){
        dataList.add(data);
    }

    public void sort(){
        dataList.sort(Comparator.comparing(Data::getName));
    }

    @Override
    public Iterator iterator() {
        return dataList.iterator();
    }
}