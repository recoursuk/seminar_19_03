import org.graalvm.compiler.lir.LIR;

import java.util.*;

public class Service {
    public static List<Data> getDataByName(List<Data> dataList, String name){
        ArrayList<Data> res = new ArrayList<Data>();
        for(Data elem:dataList){
            if(elem.getName().equals(name)){
                res.add(elem);
            }
        }
        return res;
    }

    public static List<Data> getDataWithLevel(List<Data> dataList, double level){
        ArrayList<Data> res = new ArrayList<Data>();
        for(Data elem:dataList){
            if(level - Math.abs(elem.getValue()) < 1e-9 || level > Math.abs(elem.getValue())){
                res.add(elem);
            }
        }
        return res;
    }

    public static Set<Double> getSetByNames(List<Data> dataList, Set<String> names){
        Set<Double> res = new HashSet<Double>();
        for(Data elem:dataList){
            if(names.contains(elem.getName())){
                res.add(elem.getValue());
            }
        }
        return res;
    }

    public static String[] getPositiveNames(List<Data> dataList){
        Set<String> res = new HashSet<String>();
        for (Data elem:dataList){
            if(elem.getValue() > 0 && !res.contains(elem.getName())){
                res.add(elem.getName());
            }
        }
        return (String[]) res.toArray();
    }

    public static<T> Set<T> getUnion(List<Set<T>> setList){
        Set<T> res = new HashSet<>();
        for (Set<T> set: setList){
            Collections.addAll(res, (T) set.toArray());
        }
        return res;
    }

    public static<T> Set<T> getIntersection(List<Set<T>> setList){
        Set<T> res = setList.get(0);
        for(Set<T> set: setList){
            res.retainAll(set);
        }
        return res;
    }

    public static<T> List<Set<T>> getMaxSets(List<Set<T>> setList){
        ArrayList<Set<T>> res = new ArrayList<>();
        int maxSize = 0;
        for(Set elem:setList){
            if(elem.size() > maxSize){
                maxSize = elem.size();
            }
        }
        for(Set elem:setList){
            if(elem.size() == maxSize){
                res.add(elem);
            }
        }
        return res;
    }
}
